<?php

namespace Drupal\Tests\nope\Kernel;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\nope\Commands\NopeCommands;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\paragraphs\FunctionalJavascript\ParagraphsTestBaseTrait;

class DrushNopeTest extends KernelTestBase {

  use ParagraphsTestBaseTrait;

  /**
   * @var array
   */
  protected $aliases = [];

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'entity_reference_revisions',
    'field',
    'file',
    'node',
    'paragraphs',
    'system',
    'user',
  ];

  protected $fieldName;

  protected $contentType;

  protected $paragraphType;


  protected function setUp() {
    parent::setUp();
    $this->installConfig('system');
    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('paragraph');
    $this->fieldName = strtolower($this->randomMachineName());
    $this->contentType = strtolower($this->randomMachineName());
    $this->paragraphType = strtolower($this->randomMachineName());
    $this->addParagraphedContentType($this->contentType, $this->fieldName);
    $this->addParagraphsType($this->paragraphType);
  }

  public function testNope() {
    FieldStorageConfig::loadByName('node', $this->fieldName)
      ->setTranslatable(FALSE)
      ->save();
    /** @var Paragraph[] $paragraphs */
    $paragraphs = [];
    for ($i = 0; $i < 5; $i++) {
      // id and the revision id are different to catch potential bugs mixing up
      // the two.
      $paragraphs[$i] = Paragraph::create([
        'type' => $this->paragraphType,
        'id' => 100 + $i,
        'revision_id' => 1 + $i,
      ]);
      $paragraphs[$i]->enforceIsNew()->save();
    }
    $node = Node::create([
      'type' => $this->contentType,
      'title' => $this->randomString(),
      'uid' => '0',
      'status' => 1,
      $this->fieldName => [
        $paragraphs[0],
        $paragraphs[1],
      ],
    ]);
    $node->save();
    // These two paragraphs don't get new revisions because the host is new.
    $this->assertSame(5, $this->getParagraphRevisionCount());
    $node->setNewRevision();
    $node->save();
    // Now we get two new revisions.
    $this->assertSame(7, $this->getParagraphRevisionCount());
    $node->{$this->fieldName}->setValue([
      $paragraphs[2],
      $paragraphs[3],
    ]);
    $node->setNewRevision();
    $node->save();
    // These two paragraphs get new revisions because the host is not new.
    // This creates orphans.
    $this->assertSame(9, $this->getParagraphRevisionCount());
    $node->setNewRevision();
    (new NopeCommands())->nope();
    // $paragraphs[4] is gone completely because it was never referenced and so
    // are the first two revisions of $paragraphs[2] and $paragraphs[3].
    $this->assertSame(6, $this->getParagraphRevisionCount());
    foreach ([100 => 2, 101 => 2, 102 => 1, 103 => 1, 104 => 0] as $id => $count) {
      $this->assertSame($count, $this->getParagraphRevisionCount($id));
    }
  }

  /**
   * @return array|int
   */
  protected function getParagraphRevisionCount($id = NULL) {
    $query = \Drupal::entityQuery('paragraph')
      ->allRevisions()
      ->count();
    if ($id) {
      $query->condition('id', $id);
    }
    return (int) $query->execute();
  }

}
